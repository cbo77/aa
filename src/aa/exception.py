class AA_Error(Exception):
    '''Error'''
    code = 1

class AA_Pkg_Not_Found_Error(AA_Error):
    '''Package could not be found.'''
    code = 2

class AA_Git_Error(AA_Error):
    '''Something went wrong while trying to clone a repo.'''
    code = 3

class AA_AUR_APIERROR(AA_Error):
    '''AUR search error.'''
    code = 4

class AA_Function_Not_Available(AA_Error):
    '''Attempted function not available. Are the necessary modules installed?'''
    code = 5
