import os

import aur

APT_AV = True
try:
    import apt
except ImportError:
    APT_AV = False

from aa.colours import ES_CMD, ES_USR, ES_OKY, ES_SCS, ES_ERR, ES_RST, ES_WNG, ES_SPL, ES_ALT, ES_AUR
from aa.exception import AA_Pkg_Not_Found_Error, AA_AUR_APIERROR
from aa.git import gitClone
from aa.shell import runsh


def searchAUR(term):
    try:
        results = aur.search(term)
    except aur.APIError:
        raise AA_AUR_APIERROR(f"{ES_ERR}Too many results! Try refining your search.{ES_RST}")

    if len(results):
        results = list(reversed(sorted(results, key = lambda x: x.num_votes)))
        if len(results) > 10:
            results = results[:10]
    return results


def showAURPkgInfo(pkg):
    info = dict()
    for field, value in zip(pkg._fields, pkg):
        info[field] = value

    fields = ["name", "description", "url", "num_votes"]
    names = ["Package name:", "Description:", "URL:", "Votes:"]
    
    print(f"{ES_AUR}AUR{ES_CMD} Package info{ES_RST}")
    for f, n in zip(fields, names):     
        print(f"  {ES_CMD}{n}{ES_RST}")
        print(f"    {ES_USR}{info[f]}{ES_RST}")

        
def findAURPkg(term):
    pkg = None
    try:
        pkg = aur.info(term)
    except aur.NoSuchPackageError as err:
        results = searchAUR(term)
        if len(results):
            raise AA_AUR_APIERROR(f"{ES_WNG}No package '{term}' found.{ES_RST}\n"+
                                  f"{ES_CMD}Popular search results:{ES_RST}\n"+
                                  "\n".join([f"  {ES_USR}{res.name}{ES_RST}" for res in results]))
        else:
            raise AA_AUR_APIERROR(f"{ES_ERR}{err}{ES_RST}")
    return pkg


def cloneAURRepo(pkgname, gitDir):
    gitURL = f"https://aur.archlinux.org/{pkgname}.git"
    gitClone(gitURL, gitDir, "AUR-" + pkgname)
    pkgbuild = []
    with open(gitDir + "/AUR-" + pkgname + "/PKGBUILD") as pkgbuildf:
        pkgbuild = pkgbuildf.read().split('\n')
    return pkgbuild


def cloneAURSource(pkg, gitDir):

    pkgBuild = cloneAURRepo(pkg.name, "/tmp/")

    pkgbuild = readPkgbuild(pkgBuild)

    if pkgbuild['makedepends']:
        print(f"{ES_CMD}This package depends on:{ES_RST}")
        print(f"  {ES_USR}{pkgbuild['makedepends']}{ES_RST}")

    if type(pkgbuild['source']) is list and len(pkgbuild['source']) > 1:
        # Multiple sources listed
        print(f"{ES_CMD}There are multiple sources listed for this package.{ES_RST}")
        for i, source in enumerate(pkgbuild['source']):
            print(f"  {ES_OKY}[{i}] {ES_USR}{source}{ES_RST}")
        idx = input(f"{ES_CMD}Which should be downloaded?{ES_RST} [0] ")
        idx = int(idx) if idx != '' else 0

        gitURL = pkgbuild['source'][idx]
        
    else:
        gitURL = pkgbuild['source']
        
    gitClone(gitURL, gitDir)
    return 0

def clean(string):
    return string.replace('(','').replace(')','').replace("'",'').replace('git+','')

def readPkgbuild(pkgbuild):
    pkgbuild_info = dict()

    still_reading = False
    this_key = ""
    this_value = []
    
    for line in pkgbuild:
        
        if not still_reading and (not len(line) or line[0] == " " or line[0] == "\t"): continue

        if "=" in line:
            spl = line.split('=')

            if line.count('(') > line.count(')'):
                still_reading = True
                this_key = spl[0]
                this_value.append(clean(spl[1]))
            else:
                pkgbuild_info[spl[0]] = clean(spl[1])
        elif still_reading:            
            this_value.append(line.strip())
            if line.count(')') > line.count('('):
                pkgbuild_info[this_key] = this_value
                still_reading = False
    return pkgbuild_info


def installAURPkg(pkg, gitDir):
    print(f"{ES_ERR}AUR install disabled. Downloading source...{ES_RST}")
    return cloneAURSource(pkg, gitDir)
    print(f"{ES_ALT}========================================{ES_RST}")
    print(f"{ES_CMD} Installing {ES_USR}{pkgname}{ES_RST}")
    print(f"{ES_CMD}Downloading PKGBUILD{ES_RST}")
    pkgbuild = cloneAURRepo(pkgname, gitDir)
    depends = getDependencies(pkgbuild)
    print(f"{ES_CMD}Found dependencies:{ES_RST}")
    for depend in depends:
        print(f"  {ES_USR}{depend}{ES_RST}")
    resolveDependencies(depends, gitDir)
    runPKGBUILD(pkgbuild, gitDir, pkgname)
    return


def getDependencies(pkgbuild):
    depends = []
    for line in pkgbuild:
        if line.strip().startswith("makedepends"):
            depends = line.strip()[13:-1]
    depends = [dep.replace("'", "") for dep in depends.split(" ")]
    return depends


def getAURDepends(depends):
    '''Returns list of possible AUR aliases for depends'''
    output = []
    presuf = [('',''), ('', '-git')]
    for depend in depends:
        for prfx, sufx in presuf:
            output.append((prfx+depend+sufx,depend))
    return output


def getAPTDepends(depends):
    '''Returns list of possible APT aliases for depends'''
    if not APT_AV: raise AA_Function_Not_Available(f'{ES_WNG}APT not available. Have you installed the {ES_OKY}python-apt{ES_WNG} module?{ES_RST}')
    output = []
    apt_presuf = [('',''), ('', '-bin'), ('lib', '-dev'), ('', '-dev')]
    for depend in depends:
        cnum = None
        for j, a in enumerate(depend):
            if a in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']:
                cnum = j
                break
        alt_dep = depend
        if cnum != None and alt_dep[cnum-1] != "-": alt_dep = alt_dep[:cnum] + "-" + alt_dep[cnum:]
        for prfx, sufx in apt_presuf:
            output.append((prfx+alt_dep+sufx,depend))
            output.append((prfx+depend+sufx,depend))
    return output


def resolveDependencies(depends, gitDir):
    print(f"{ES_CMD}Resolving dependencies{ES_RST}")

    ### CHECK APT ###
    if APT_AV:
        print(f"{ES_CMD}Checking APT Cache...{ES_RST}")
        apt_install_list = []
        cache = apt.Cache()
        dependsleft = ' '.join(depends)
    
        for apt_dep, depend in getAPTDepends(depends):
            try:
                p = cache[apt_dep]
                if not p.is_installed and not apt_dep in apt_install_list: apt_install_list.append(apt_dep)
                if depend in dependsleft: dependsleft = dependsleft.replace(depend, '')
            except KeyError:
                pass
    
        if len(apt_install_list):
            print(f"{ES_CMD}Installing depends from APT:{ES_RST}")
            for aptin in apt_install_list:
                print(f"  {ES_USR}{aptin}{ES_RST}")
            c, __, __ = runsh("sudo apt install "+" ".join(apt_install_list), redir_stdout=False, redir_stderr=False)
            if c:
                print(f"{ES_ERR}Error during apt install of dependencies!{ES_RST}")
        else:
            print(f"  {ES_OKY}Nothing needed!{ES_RST}")
    else:
        ### CHECK AUR ###
        print(f"{ES_CMD}Checking AUR...{ES_RST}")
        for aur_dep, depend in getAURDepends(depends):
            if depend not in dependsleft: continue
            try:
                pkg = findAURPkg(aur_dep)
                installAURPkg(pkg.name, gitDir)
                if depend in dependsleft: dependsleft = dependsleft.replace(depend, '')
            except AA_Pkg_Not_Found_Error as err:
                print(f"  {ES_USR}{aur_dep}{ES_WNG} not found{ES_RST}")
            except AA_AUR_APIERROR as err:
                print(f"{ES_ERR}{err}{ES_RST}")
    return

def runPKGBUILD(pkgbuild, gitDir, pkgname):
    pkgbuilddir = gitDir + "AUR-" + pkgname + "/"
    mkpkgclone_contents = '''#!/bin/sh
. ./PKGBUILD

prepare
build
package
'''
    os.chdir(pkgbuilddir)
    
    with open("mkpkg.sh", "w") as mkpkgf:
        mkpkgf.write(mkpkgclone_contents)

    runsh("sudo sh mkpkg.sh", redir_stdout=False, redir_stderr=False)
