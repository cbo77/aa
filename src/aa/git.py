import os

from aa.shell import runsh
from aa.colours import ES_CMD, ES_USR, ES_OKY, ES_SCS, ES_ERR, ES_RST, ES_WNG, ES_SPL

def gitClone(gitURL, gitDir, destName=""):
    print(f"{ES_CMD}Moving to download directory {ES_USR}'{gitDir}'{ES_RST}")
    os.chdir(gitDir)
    print(f"{ES_CMD}Cloning git repository {ES_USR}'{gitURL}'\n{ES_RST}",end='')
    c, __, __ = runsh(f"git clone --recursive {gitURL} {destName}", redir_stdout=False, redir_stderr=False)
    if c:
        if c == 128:
            print(f"{ES_WNG}Git repository already exists.{ES_RST}")
        else:
            print(f"{ES_ERR}Something went wrong!{ES_RST}")
            print(f"{ES_CMD}Could not download. Exiting.{ES_RST}")
            exit(1)
    return 0
