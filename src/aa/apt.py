import apt

from aa.exception import AA_Pkg_Not_Found_Error
from aa.colours import ES_CMD, ES_USR, ES_OKY, ES_SCS, ES_ERR, ES_RST, ES_WNG, ES_SPL, ES_APT
from aa.shell import runsh

def findAPTPkg(searchTerm):
    cache = apt.Cache()
    try:
        pkg = cache[searchTerm]
        return pkg
    except KeyError as err:
        raise AA_Pkg_Not_Found_Error(err)

def installAPTPkg(pkg):
    print(f"{ES_CMD}Installing package {pkg.name}{ES_RST}")
    runsh(f"sudo apt install {pkg.name}", redir_stdout=False, redir_stderr=False)

def showAPTPkgInfo(pkg):
    print(f"{ES_APT}APT{ES_CMD} Pakcage Info{ES_RST}")
    print(f"  {ES_CMD}Name{ES_RST}")
    print(f"    {ES_USR}{pkg.name}{ES_RST}")
    print(f"  {ES_CMD}Description{ES_RST}")
    print(f"    {ES_USR}{pkg.versions[0].raw_description}{ES_RST}")
    print(f"  {ES_CMD}Version{ES_RST}")
    print(f"    {ES_USR}{pkg.versions[0].source_version}{ES_RST}")
