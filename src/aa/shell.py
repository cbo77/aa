import subprocess as sp

def runsh(command, redir_stdout=True, redir_stderr=True):
    params = {"shell":True}
    if redir_stdout: params["stdout"] = sp.PIPE
    if redir_stderr: params["stderr"] = sp.PIPE

    pr = sp.Popen(command, **params)
    rc = pr.wait()

    stdout = "" if not redir_stdout else pr.stdout.read().decode("utf-8").strip()
    stderr = "" if not redir_stderr else pr.stderr.read().decode("utf-8").strip()
    
    return rc, stdout, stderr
