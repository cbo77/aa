#!/usr/bin/env python3
'''
aa

Anywhere AUR

Search and (try to) install things from the Arch User Repository even from 
distros that are not Arch!

Written because of the shear frustration at being forced to use Ubuntu coming
from an Arch install, losing easy access to the AUR is the biggest hardship.

Usage:
  aa (info | source | install) <input> [--git-dir=<git-dir>] [--only-aur | --only-apt]

Options:
  info                 Returns information about the package.
  source               Will try to download the source git repo.
  install              Will download the aur repo and execute the PKGBUILD
  <input>              The search term.
  --git-dir=<git-dir>  The directory to store downloads [Default: $HOME/ro_gits/]
  --only-aur           Only search the AUR for packages
  --only-apt           Only search APT repositories for packages
'''

# TODO: DEPENDENCY RESOLUTION: TRY DIFFERENT PERMUTATIONS OF pkg.name + "-git" and stuff
# TODO: DEPENDENCY FIDNING: GET IF PKG IS ALREADY INSTALLED
# TODO: CREATE/UPDATE A LIST OF AUR APPS INSTALLED

from docopt import docopt

from aa.exception import AA_Pkg_Not_Found_Error, AA_AUR_APIERROR
from aa.colours import ES_CMD, ES_USR, ES_OKY, ES_SCS, ES_ERR, ES_RST, ES_WNG, ES_SPL, ES_AUR, ES_APT
from aa.shell import runsh
from aa.package import showPkgInfo, installPkg, findPkg

SOURCES = []
try:
    import aur
    SOURCES.append('AUR')
except ImportError as err:
    pass
try:
    import apt
    SOURCES.append('APT')
except ImportError as err:
    pass
    

def getSplash():
    splash = ES_SPL
    splash += "_________________________________________________________________________________\n"
    splash += "    __                                                    __     _     _   ____  \n"
    splash += "    / |                       /                           / |    /    /    /    )\n"
    splash += "---/__|----__----------------/__----__---)__----__-------/__|---/----/----/___ /-\n"
    splash += "  /   |  /   ) /   /| /| /  /   ) /___) /   ) /___)     /   |  /    /    /    |  \n"
    splash += "_/____|_/___/_(___/_|/_|/__/___/_(___ _/_____(___ _____/____|_(____/____/_____|__\n"
    splash += "                 /                                                               \n"
    splash += "             (_ /     " + ES_CMD + "Search the AUR from anywhere!" + ES_RST + "\n"
    return splash


def getSmallSplash():
    splash = f"{ES_SPL}A{ES_CMD}nywhere {ES_SPL}A{ES_CMD}UR{ES_RST}"
    return splash


def main():
    global __doc__
    global SOURCES
    splash = getSplash()
    smallSplash = getSmallSplash()
    __doc__ = __doc__.replace("Anywhere AUR", splash)
    args = docopt(__doc__, version="0.0.1")
    
    searchTerm = args['<input>']
    __, gitDir, __ = runsh("echo "+args['--git-dir'])

    print(smallSplash)

    if len(SOURCES) < 2:
        if 'APT' in SOURCES:
            print(f"{ES_WNG}Warning: APT not available, some features may not work as intended.{ES_RST}")
        elif 'AUR' in SOURCES:
            print(f"{ES_WNG}Warning: APT not available, some features may not work as intended.{ES_RST}")
        else:
            print(f"{ES_ERR}Error: No sources available!{ES_RST}\n{ES_WNG}(Have you installed the 'apt' or 'aur' python modules?){ES_RST}")
            
    if args['--only-aur']:
        if 'AUR' in SOURCES:
            SOURCES = ['AUR']
        else:
            print(f"{ES_ERR}Cannot use AUR as sole source!{ES_RST}")
            print(f"{ES_WNG}Source is not available{ES_RST}")
            exit(1)
    elif args['--only-apt']:
        if 'APT' in SOURCES:
            SOURCES = ['APT']
        else:
            print(f"{ES_ERR}Cannot use APT as sole source!{ES_RST}")
            print(f"{ES_WNG}Source is not available{ES_RST}")
            exit(1)

    if not 'AUR' in SOURCES and args['source']:
        print(f"{ES_ERR}You must enable AUR searching to download source repositories!{ES_RST}")
        exit(1)

    pkg_res = [False, False]
    print(f"{ES_CMD}Searching for '{searchTerm}'...{ES_RST}")
    cols = {"AUR":ES_AUR, "APT":ES_APT}
    for i, source in enumerate(SOURCES):
        print(f"{ES_CMD}[{cols[source]}{source}{ES_CMD}] {ES_RST}", end='')
        try:
            pkg_res[i] = (findPkg(searchTerm, source), source)
            print(f"{ES_OKY}found!{ES_RST}")
        except AA_AUR_APIERROR as err:
            print(f"{ES_WNG}search could not be completed: \n\t{err}{ES_RST}")
        except AA_Pkg_Not_Found_Error as err:
            print(f"{ES_WNG}not found.{ES_RST}")
            
    if not [res for res in pkg_res if res]:
        print(f"{ES_ERR}Package not found!{ES_RST}")
        exit(1)
    
    if args['info']:
        for pkg, source in [res for res in pkg_res if res]:
            showPkgInfo(pkg, source)
    elif args['source']:
        if pkg_res[1]: cloneAURSource(pkg_res[1][0], gitDir)
        else:
            print(f"{ES_ERR}Could not retrieve package source: no AUR package found.{ES_RST}")
            exit(1)
    elif args['install']:
        pkg_res = [res for res in pkg_res if res]

        if len(pkg_res) > 1:
            print(f"{ES_CMD}There are multiple possible package sources to install from.{ES_RST}")
            for i, (pkg, source) in enumerate(pkg_res):
                print(f"  {ES_OKY}[{i}] {ES_USR}{source}{ES_RST}")
            idx = input(f"{ES_CMD}From which would you like to install?{ES_RST} [0]")
            idx = int(idx) if idx != '' else 0
            pkg_res = pkg_res[idx]
        else:
            pkg_res = pkg_res[0]
            
        installPkg(*pkg_res, gitDir)
