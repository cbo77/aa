AUR_AV = True
APT_AV = True

try:
    from aa.aur import installAURPkg, showAURPkgInfo, findAURPkg
except ImportError as err:
    AUR_AV = False
try:
    from aa.apt import installAPTPkg, showAPTPkgInfo, findAPTPkg
except ImportError:
    APT_AV = False

def installPkg(pkg, source, gitDir):
    if source == "APT":
        return installAPTPkg(pkg) if APT_AV else 0
    elif source == "AUR":
        return installAURPkg(pkg, gitDir) if AUR_AV else 0
    else:
        return 0

def showPkgInfo(pkg, source):
    if source == "APT":
        return showAPTPkgInfo(pkg) if APT_AV else 0
    elif source == "AUR":
        return showAURPkgInfo(pkg) if AUR_AV else 0
    else:
        return 0

def findPkg(searchTerm, source):
    if source =="APT":
        return findAPTPkg(searchTerm) if APT_AV else 0
    elif source == "AUR":
        return findAURPkg(searchTerm) if AUR_AV else 0
    else:
        return 0
