"""

"""

from codecs import open
from os import path

from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='aa',

    version='0.0.1',  # do not forget to change this

    python_requires='>=3.6',

    description='AUR from Ubuntu',

    long_description=long_description,

    url='https://gitlab.com/cbo77/aa',

    author='Christopher Boyle',

    author_email='',

    packages=find_packages('src'),
    package_dir={'': 'src'},

    entry_points={
        'console_scripts': [
            'aa=aa.main:main'
        ]
    },

    install_requires=[]
)
